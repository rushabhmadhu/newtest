jQuery(document).ready(function() {
    console.log("Welcome");
    var input = document.getElementById("searchBox");
    input.addEventListener("keyup", function(event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            document.getElementById("searchForm").submit();
        } else {
            var searchVal = $.trim($("#searchBox").val());
            if (searchVal.length > 0) {
                $(".error-box").html("");
            }
        }
    });
})

function validateSearch() {
    var searchVal = $.trim($("#searchBox").val());

    if (searchVal.length > 0) {
        return true;
    } else {
        $(".error-box").html("Please enter search text");
        $(".error-box").css('color', '#ff0000');
    }

    return false;
}