<?php 
require_once 'config/configuration.php';
$resumes = fetchCandidates($_REQUEST);
$value = (isset($_GET['search']))? $_GET['search'] : '';
?>
<html>
  <head>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <script src="js/custom.js"></script>
  </head>
  <body>
    <div class="container">
    <div class="jumbotron">
      <h1 class="display-4">Welcome to HRMS</h1>
      <p class="lead">This is a listing of all resume information.</p>
      <hr class="my-4">
      <p>Thanks to be with us.</p>
    </div>
	    <div class="row">
        <div class="col-md-12">
          <h2>Search for Candidate</h2>
          <p>Search by candidate name, email or phone number for Accurate results search with proper information like fullname or full email or full mobile numbr:</p>  
          <form method="get" id="searchForm" onsubmit="return validateSearch()">
            <input class="form-control" id="searchBox" type="text" placeholder="Search.." name="search" value="<?php echo $value;?>"> <br />
            <label class="error-box"></label> <br />
            <input type="submit" id="submitBtn" name="submit" value="Search" class="btn btn-primary" />
          </form>
          <br>
        </div>
      </div>
	    <div class="row">
        <div class="col-md-12">
        <h4>Resume Information</h4>
          <div class="table-responsive">                
              <table id="mytable" class="table table-bordred table-striped table-hover">
                <thead>                   
                  <th>ID</th>
                  <th>Candidate Name</th>
                  <th>Email</th>
                  <th>Mobile</th>
                  <th>Register Date</th>
                  <th>Download</th>                  
                </thead>
                <tbody>
                <?php 
                foreach($resumes as $key => $resume) { ?>
                  <tr>
                    <td><?php echo $key+1; ?></td>
                    <td><?php echo $resume['vCandidateName']; ?></td>
                    <td><?php echo $resume['vEmail'];?></td>
                    <td><?php echo $resume['vMobile'];?></td>
                    <td><?php echo date('Y-m-d h:i:a', $resume['iCreatedAt']);?></td>
                    <td><?php echo '<a href="./uploads/'.$resume['vResumeDoc'].'" target="_new" /> Download CV </a>';?></td>  
                  </tr>   
                <?php } ?>
                </tbody>       
              </table>           
          </div>            
        </div>
	    </div>
    </div>
    </div>
  </body>
</html>