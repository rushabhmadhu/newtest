<?php
require_once '../config/configuration.php';

$response = ['response_code'=>400,'response_message'=>'Invalid params.'];

if($_GET["action"]=="register-cv") {
    
    //Store Post Params to Local Variable
    $postData = $_POST;
    
    //Validate Fields
    $bIsSet = isset($postData['candidateName']);
    $bHasLength = (strlen(trim($postData['candidateName']))>0);
    
    if($bIsSet==false || $bHasLength==false) {
        $response['errors'] = ['candidateName' => 'Candiate Name is required']; goto returnResponse;
    }
    if(!(isset($postData['email']) && strlen(trim($postData['email']))>0)) {
        $response['errors'] = ['email' => 'Email is required']; goto returnResponse;
    } else {
        if (!(filter_var($postData['email'], FILTER_VALIDATE_EMAIL))) {
            $response['errors'] = ['email' => 'Valid Email is required']; goto returnResponse;
        }
    }
    if(!(isset($postData['mobile']) && strlen(trim($postData['mobile']))>0)) {
        $response['errors'] = ['mobile' => 'Mobile is required']; goto returnResponse;
    }
    
    //Validate File
    if(isset($_FILES["resume"])) {
        $fileInfo = (pathinfo($_FILES['resume']['name']));
        $fewFileName = trim(substr($fileInfo['filename'],0,10))."_".time().".".$fileInfo['extension'];
        $postData['vResumeDoc'] = $fewFileName;
        if(!(in_array(strtolower($fileInfo['extension']),['pdf','doc','docx']))) {
            $response['errors'] = ['vResumeDoc' => 'Resume Doc is missing']; 
            goto returnResponse;
        }
    }

    $postData['iCreatedAt'] = time();
    $postData['iSourceId'] = getallheaders()['source']; //get_client_ip();    

    $response = addResume($postData, $response);
} 
returnResponse:
echo json_encode($response);die;
?>