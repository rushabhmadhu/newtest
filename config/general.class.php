<?php

class general{
    
    public $connection = null;
    public $db_name = "newtest";
    public $db_user = "root";
    public $db_pass = "ur48x";
    public $host_nm = "localhost";
    public $port = "3306";
    public $charset = 'utf8';

    function __construct(){
        $options = [ \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
                    \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
                    \PDO::ATTR_EMULATE_PREPARES   => false,
                    ];        
        $dsn = "mysql:host=$this->host_nm;dbname=$this->db_name;charset=$this->charset;port=$this->port";
        
        try {
            $pdo = new \PDO($dsn, $this->db_user, $this->db_pass, $options);
        } catch (\PDOException $e) {
            throw new \PDOException($e->getMessage(), (int)$e->getCode());
        }        
        $this->setConnection($pdo);        
    }

    public function getConnection() {
        return $this->connection;
    }

    public function setConnection($connection) {
        $this->connection = $connection;
    }

	function do_trim( $str, $replace_by = "-" ){
		return trim( $str ) ? $str : $replace_by;
	}
	
	function pathImage($name){ return SITE_URL."images/".$name; }
	
	function check_login(){
		if( !AID ){ $this->redirectTo('index.php'); }
	}

	function redirectTo($redirect_url){
		@header("location: {$redirect_url}");
		echo "<script type=\"text/javascript\">location.href = \"{$redirect_url}\"</script>";
		die();
	}

    function file_get_contents_curl($url, $data) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_URL, $url);
        if( !empty($data) ){
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        }
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }
}
?>