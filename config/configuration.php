<?php 
session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ERROR);
date_default_timezone_set('Asia/Kolkata'); 

function _p( $arr ){
	echo '<pre>'; print_r( $arr ); echo '</pre>';
}

define( "CURR_DOMAIN", str_replace( "www.", "", $_SERVER['HTTP_HOST'] ) );
define( "BASE_FILE", basename( $_SERVER['PHP_SELF'] ) );

require_once( "general.class.php" );

function fetchCandidates($searchParam) {
	$gnrl = new general();
	$conn = $gnrl->getConnection();
	if(isset($searchParam['search']) && strlen($searchParam['search'])>0) {
		$searchValue = '%'.$searchParam['search'].'%';
		$sth  = $conn->prepare("SELECT * FROM resumes where (vCandidateName LIKE ? OR vEmail LIKE ? OR vMobile LIKE ?)");
		$sth->execute([$searchValue, $searchValue, $searchValue]);
		$result = $sth->fetchAll();		
	} else {
		$sth  = $conn->prepare("SELECT * FROM resumes");
		$sth->execute();
		$result = $sth->fetchAll();
	}
	return $result;	
}

function addResume($postData, $response) {
	
	//Get Connection
	$gnrl = new general();
    $conn = $gnrl->getConnection();
	
	try {	
		//Check for Existing Resume
		$stmt = $conn->prepare("SELECT iResumeId FROM resumes WHERE vEmail=? OR vMobile=?");
		$stmt->execute([$postData['email'],$postData['mobile']]);
		if($stmt->rowCount()>0) {
			$response['errors'] = ['email' => 'Resume already exist with same Email or mobile'];
			return $response;
		}    
		
		//Validate and Fetch Source
		$postData['iSourceId'] = validateSource($postData['iSourceId']);
		if($postData['iSourceId'] == 0) {
			$response['response_message'] = 'You are not allowed to access from Here';
        	$response['errors']['exception'] = 'Invalid source or endpoints used';
		}

		//Register new Resume to Database
		$statement = $conn->prepare('INSERT INTO resumes (vCandidateName, vEmail, vMobile, vResumeDoc, iSourceId, iCreatedAt) VALUES (:vCandidateName, :vEmail, :vMobile, :vResumeDoc, :iSourceId, :iCreatedAt)');
		$statement->execute([
			"vCandidateName" => $postData['candidateName'],
			"vEmail" => $postData['email'],
			"vMobile" => $postData['mobile'],
			"vResumeDoc" => $postData['vResumeDoc'],
			"iSourceId" => $postData['iSourceId'],
			"iCreatedAt" => $postData['iCreatedAt']
		]);
		
		// Upload Resume Files
		$folderPath = "../uploads";            
		move_uploaded_file($_FILES['resume']['tmp_name'], $folderPath.'/'.$postData['vResumeDoc']);

		$response['response_message'] = 'Your resume submitted successfully';
		$response['response_code'] = 200;
		$response['response_data'] = $postData;
	} catch (Exception $ex) {
        $response['response_message'] = $ex->getMessage();
        $response['errors']['exception'] = 'Exception handeled';
    }
	return $response;
}

function validateSource($source) {
	$gnrl = new general();
	$conn = $gnrl->getConnection();
	$stmt = $conn->prepare("SELECT iSourceId FROM allowed_source WHERE vSourceIP=?  ORDER BY iSourceId DESC LIMIT 1");
	$stmt->execute([$source]);
	$rowCount = $stmt->rowCount();
	
	if($rowCount==0) {
		return $rowCount;
	} else {
		$row = $stmt->fetch();
		return $row['iSourceId'];
	}		
}

// Function to get the client IP address
function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}